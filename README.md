# Jaba Authorization Submodule



## Getting started

To make project running it is necessary to have some kind of local mail server running

In this project used mail server is MailDev

## MailDev configuration
Sources can be found on https://github.com/maildev/maildev

To get MailDev running via nodeJS it may require to change `ExecutionPolicy` on Windows to `Unrestricted`

![](https://gitlab.com/yaroslav833/jaba-authorization-submodule/-/raw/main/instr.png?ref_type=heads)

MailDex launching from nodeJS is performed via `maildev` command