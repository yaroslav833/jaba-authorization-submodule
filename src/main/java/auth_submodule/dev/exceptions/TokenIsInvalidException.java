package auth_submodule.dev.exceptions;

// ToDo: InvalidTokenException, Ctrl+Alt+L, remove redundant lines
public class TokenIsInvalidException extends RuntimeException{

    public TokenIsInvalidException(String message) {
        super(message);
    }

}