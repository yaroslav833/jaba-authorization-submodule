package auth_submodule.dev.exceptions;

// ToDo: UserNotExistsException, Ctrl+Alt+L
public class UserDoesNotExistException extends RuntimeException{
    public UserDoesNotExistException(String message) {
        super(message);
    }
}
