package auth_submodule.dev.exceptions;

public class EmailNotSentException extends RuntimeException{ // ToDo: Ctrl+Alt+L
    public EmailNotSentException(String message) {
        super(message);
    }
}