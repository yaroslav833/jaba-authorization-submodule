package auth_submodule.dev.user.services;

import auth_submodule.common.constants.ExceptionConstants;
import auth_submodule.dev.exceptions.EmailAlreadyTakenException;
import auth_submodule.dev.token.entities.ConfirmationTokenEntity;
import auth_submodule.dev.registration.services.ConfirmationTokenService;
import auth_submodule.dev.user.entity.UserEntity;
import auth_submodule.dev.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder; // ToDo: here we need to put an interface (PasswordEncoder)
    private final ConfirmationTokenService confirmationTokenService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException { // ToDo: redundant throws
        return userRepository
                .findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException(String.format(ExceptionConstants.USER_NOT_FOUND_MESSAGE, email)));
    }

    public String signUpUser(UserEntity userEntity) {
        boolean userExists = userRepository // ToDo: boolean and if condition can be extracted to separate method, try to combine both into one stream
                .findAll()
                .stream()
                .anyMatch(user -> user.getEmail().equals(userEntity.getEmail()));

        if (userExists) {
            throw new EmailAlreadyTakenException(ExceptionConstants.EMAIL_ALREADY_TAKEN);
        }

        savePassword(userEntity);

        return getToken(userEntity);
    }

    public void enableAppUser(String email) {
        userRepository.enableAppUser(email);
    } // ToDo: change the name

    private void savePassword(UserEntity userEntity) {
        String encodedPassword = bCryptPasswordEncoder.encode(userEntity.getPassword());
        userEntity.setPassword(encodedPassword);

        userRepository.save(userEntity);
    }

    private String getToken(UserEntity userEntity) {
        String token = UUID.randomUUID().toString();
        ConfirmationTokenEntity confirmationTokenEntity = new ConfirmationTokenEntity( // ToDo: move to separate mapper
                token,
                userEntity
        );

        saveConfirmationToken(confirmationTokenEntity);

        return token;
    }

    private void saveConfirmationToken(ConfirmationTokenEntity confirmationTokenEntity) {
        confirmationTokenService.saveConfirmationToken(confirmationTokenEntity);
    }

}