package auth_submodule.dev.user.entity;

import auth_submodule.common.constants.DatabaseConstants;
import auth_submodule.common.security.UserRole;
import auth_submodule.dev.authentication.JwtAuthenticationFilter;
import auth_submodule.dev.token.entities.AccessTokenEntity;
import auth_submodule.dev.token.entities.RefreshTokenEntity;
import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.NonNull;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

/**
 * UserDetails interface is implemented to enable user processing
 * with {@link org.springframework.security.core.context.SecurityContextHolder}
 * as well as to perform request filtering while taking into consideration
 * granted authorities for the specific user in
 * {@link JwtAuthenticationFilter}
 */
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor(force = true) // ToDo: redundant (force = true)
@Entity(name = DatabaseConstants.REGISTRATION_TABLE_NAME)
public class UserEntity implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Column(name = "first_name", nullable = false) // ToDo: remove name
    private String firstName;

    @NonNull
    @Column(name = "last_name", nullable = false) // ToDo: remove name
    private String lastName;

    @NonNull
    @Column(name = "email", nullable = false) // ToDo: remove name
    private String email;

    @NonNull
    @Column(name = "password", nullable = false) // ToDo: remove name
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "user_role")  // ToDo: remove name, move @Column above @Enumerated, add @NonNull, nullable = false
    private UserRole userRole;

    @OneToMany(mappedBy = "user")
    private List<AccessTokenEntity> tokens; // ToDo: rename to accessTokens

    @OneToMany(mappedBy = "user")
    private List<RefreshTokenEntity> refreshToken; // ToDo: rename to refreshTokens

    @Column(name = "locked", nullable = false) // ToDo: remove name
    private Boolean locked; // ToDo: Boolean to boolean

    @Column(name = "enabled", nullable = false) // ToDo: remove name
    private Boolean enabled; // ToDo: Boolean to boolean

    public UserEntity(@NonNull String firstName,
                      @NonNull String lastName,
                      @NonNull String email,
                      @NonNull String password,
                      UserRole userRole) { // ToDo: @NonNull
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.userRole = userRole;

        this.locked = false;
        this.enabled = false;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return userRole.getAuthorities();
    }

    @NonNull
    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !locked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

}