package auth_submodule.dev.authentication.controllers;

import auth_submodule.dev.authentication.controllers.dto.AuthenticationRequest;
import auth_submodule.dev.token.dto.TokenDTO;
import auth_submodule.dev.authentication.services.AuthenticationService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/auth")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping
    public ResponseEntity<TokenDTO> authenticate(
            @RequestBody AuthenticationRequest request) { // ToDo: @Valid, Ctrl+Alt+L, make param in one line with method signature
        return ResponseEntity.ok(authenticationService.authenticate(request)); // ToDo: extract result of .authenticate() to variable
    }

    @PostMapping("/refresh-token")
    public void refreshToken(
            HttpServletRequest request,
            HttpServletResponse response) throws IOException { // ToDo: make param in one line with method signature, use try catch inside .refreshToken()
        authenticationService.refreshToken(request, response);
    }

}