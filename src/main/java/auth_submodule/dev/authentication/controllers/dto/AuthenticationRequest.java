package auth_submodule.dev.authentication.controllers.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder // ToDo: redundant
@AllArgsConstructor // ToDo: pyramid approach
@NoArgsConstructor
public class AuthenticationRequest { // ToDo: rename to AuthenticationDTO

    private String email; // ToDo: remove redundant lines, add @NotBlank annotation

    private String password; // ToDo: remove redundant lines, add @NotBlank annotation

}