package auth_submodule.dev.authentication;

import auth_submodule.dev.authentication.services.JwtService;
import auth_submodule.dev.token.repositories.AccessTokenRepository;
import auth_submodule.dev.user.services.UserService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter { // ToDo: must be located in security package in separate 'filters' package near configurations

    private final JwtService jwtService; // TODo: remove redundant lines

    private final UserService userService;

    private final AccessTokenRepository accessTokenRepository;

    // ToDo: protected void doFilterInternal(HttpServletRequest request,
    //                                       HttpServletResponse response,
    //                                       FilterChain filterChain)
    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response, // ToDo: Alt+Enter
            FilterChain filterChain // ToDo: Alt+Enter
    ) throws ServletException, IOException { // ToDo: similar comments as in AuthenticationService regarding refreshToken
        final String authHeader = request.getHeader("Authorization");
        final String jwt;
        final String userEmail;
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            filterChain.doFilter(request, response);
            return;
        }
        jwt = authHeader.substring(7);
        userEmail = jwtService.extractUsername(jwt);
        if (userEmail != null && SecurityContextHolder.getContext().getAuthentication() == null) { // ToDo: can be extracted to separate method
            UserDetails userDetails = userService.loadUserByUsername(userEmail);

            boolean isTokenValid = accessTokenRepository.findByToken(jwt)
                    .map(t -> !t.isExpired() && !t.isRevoked())
                    .orElse(false);

            // ToDo: jwtService.isTokenValid(jwt, userDetails) && isTokenValid - don't understand
            if (jwtService.isTokenValid(jwt, userDetails) && isTokenValid && userDetails.isEnabled()) { // ToDo: why to check two times if token is valid
                final UsernamePasswordAuthenticationToken authToken =
                        new UsernamePasswordAuthenticationToken(
                                userDetails,
                                null, // ToDo: remind why credentials = null
                                userDetails.getAuthorities());
                authToken.setDetails(
                        new WebAuthenticationDetailsSource().buildDetails(request)); // ToDo: extract to separate variable
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        }
        filterChain.doFilter(request, response);
    }

}