package auth_submodule.dev.authentication.services;

import auth_submodule.common.signin.services.SignInService;
import auth_submodule.dev.authentication.controllers.dto.AuthenticationRequest;
import auth_submodule.dev.token.dto.TokenDTO;
import auth_submodule.dev.exceptions.TokenIsInvalidException;
import auth_submodule.dev.exceptions.UserDoesNotExistException;
import auth_submodule.dev.token.repositories.AccessTokenRepository;
import auth_submodule.dev.token.repositories.RefreshTokenRepository;
import auth_submodule.dev.user.entity.UserEntity;
import auth_submodule.dev.user.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class AuthenticationService extends SignInService {

    private final AuthenticationManager authenticationManager; // ToDo: pyramid approach

    private final UserRepository userRepository; // ToDo: respective repositories must be handled in respective services, here inject UserService

    public AuthenticationService( // ToDo: after SignInService is removed, you can use @RequiredArgsConstructor
            JwtService jwtService,
            UserRepository userRepository,
            AccessTokenRepository accessTokenRepository,
            AuthenticationManager authenticationManager,
            RefreshTokenRepository refreshTokenRepository) {
        super(jwtService, accessTokenRepository, refreshTokenRepository);
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
    }

    public TokenDTO authenticate(
            AuthenticationRequest request) { // ToDo: one line method signature
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken( // ToDo: extract creation of AuthToken into separate variable
                request.getEmail(), request.getPassword()
        ));

        final var user = userRepository.findByEmail(request.getEmail()) // ToDo: enter
                .orElseThrow(() -> new UserDoesNotExistException("user with username " + request.getEmail() + " does not exist"));

        final String jwtToken = jwtService.generateAuthorizationToken(user);
        final String refreshToken = getRefreshToken(user);

        revokeAllAccessTokens(user);
        saveUserToken(user, jwtToken);
        //saveRefreshToken(user, refreshToken); // ToDo: redundant

        return TokenDTO.builder()
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .build(); // ToDo: can be moved to TokenMapper
    }

    public void refreshToken(
            HttpServletRequest request,
            HttpServletResponse response) // ToDo: params to one line
            throws IOException { // ToDo: try-catch in this method instead of throws
        final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        final String refreshToken; // ToDo: init variable below the if condition
        final String userEmail; // ToDo: init variable below the if condition
        if (authHeader == null || !authHeader.startsWith("Bearer ")) { // ToDo: similar issue as was earlier, extract condition to separate method
            return;
        }
        refreshToken = authHeader.substring(7); // ToDo: similar
        userEmail = jwtService.extractUsername(refreshToken);
        if (userEmail != null ) { // ToDo: Ctrl+Alt+L, can be extracted to separate method
            UserEntity userEntity = userRepository.findByEmail(userEmail) // ToDo: Enter
                    .orElseThrow(() -> new UserDoesNotExistException("user with username " + userEmail + " does not exist")); // ToDo: message can be moved to ExceptionConstants

            var refreshTokenEntity = refreshTokenRepository.findByToken(refreshToken) // ToDo: Enter
                    .orElseThrow(() -> new TokenIsInvalidException("refresh token " + refreshToken + " is invalid")); // ToDo: message can be moved to ExceptionConstants

            if(refreshTokenEntity.isExpired() || refreshTokenEntity.isRevoked()) { // ToDo: can be extracted to void validateRefreshToken
                throw new TokenIsInvalidException("refresh token " + refreshToken + " is invalid"); // ToDo: message can be moved to ExceptionConstants
            }

            if (jwtService.isTokenValid(refreshToken, userEntity)) { // ToDo: in previous line we validate refreshTokenEntity, here we validate refreshToken, what's the difference?
                var accessToken = jwtService.generateAuthorizationToken(userEntity);
                revokeAllAccessTokens(userEntity);
                saveUserToken(userEntity, accessToken);

                var authResponse = TokenDTO.builder()
                        .accessToken(accessToken)
                        .refreshToken(refreshToken)
                        .build(); // ToDo: move to separate mapper
                new ObjectMapper().writeValue(response.getOutputStream(), authResponse); // ToDo: you can put ObjectMapper as a bean, and inject it if needed
            }
        }
    }

}