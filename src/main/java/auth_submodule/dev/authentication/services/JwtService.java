package auth_submodule.dev.authentication.services;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtService {

    @Value("${jwt-service.signing-key.secret}")
    private String secretKey; // ToDo: remove redundant lines

    @Value("${jwt-service.signing-key.expiration}")
    private long jwtExpirationTime; // ToDo: add qualifier if the value is in Ms, S, M, H at the end of the variable

    @Value("${jwt-service.refresh-token.expiration}")
    private long refreshExpirationTime; // ToDo: add qualifier if the value is in Ms, S, M, H at the end of the variable

    public String extractUsername(String authorizationToken) {
        return extractClaim(authorizationToken, Claims::getSubject);
    }

    public <T> T extractClaim(String authorizationToken, Function<Claims, T> claimsResolver) { // ToDo: can be private
        final Claims claims = extractAllClaims(authorizationToken);
        return claimsResolver.apply(claims);
    }

    public String generateAuthorizationToken(UserDetails userDetails) {
        return generateAuthorizationToken(new HashMap<>(), userDetails);
    }

    // ToDo: JwtService works particularly with tokens. Mentioning Access Token or Refresh Token shouldn't be allowed here.
    //  Try to make it like this - public String generateToken()

    public String generateAuthorizationToken(Map<String, Object> extraClaims, UserDetails userDetails) { // ToDo: can be private
        return buildToken(extraClaims, userDetails, jwtExpirationTime);
    } // ToDo: Ctrl+Alt+L, indent
    public String generateRefreshToken(UserDetails userDetails) {
        return buildToken(new HashMap<>(), userDetails, refreshExpirationTime);
    }

    public boolean isTokenValid(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token)); // ToDo: redundant parenthesis
    }

    // ToDo: private String buildToken(Map<String, Object> extraClaims,
    //                                 UserDetails userDetails,
    //                                 long expirationTime) {
    private String buildToken(
            Map<String, Object> extraClaims,
            UserDetails userDetails,
            long expirationTime) {
        return Jwts
                .builder()
                .setClaims(extraClaims)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis())) // ToDo: extract to separate variable
                .setExpiration(new Date(System.currentTimeMillis() + expirationTime))  // ToDo: extract to separate variable, I think it should use issuedAt variable
                .signWith(getSigningKey(), SignatureAlgorithm.HS256)
                .compact();
    }

    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    private Claims extractAllClaims(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getSigningKey()) // ToDo: personally was concerned about pushing getSigningKey() within setSigningKey(), but in this case it's ok as we don't want to store secret data during runtime
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    private Key getSigningKey() {
        byte[] keyBytes = Decoders.BASE64.decode(secretKey);
        return Keys.hmacShaKeyFor(keyBytes);
    }

}