package auth_submodule.dev.check;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/demo-controller")
public class TestController {

    @GetMapping
    @RequestMapping("/admin") // ToDo: @GetMapping("/admin")
    @PreAuthorize("hasAuthority('admin:read')") // ToDo: example - @PreAuthorize("hasRole('" + IP_CONFIGURATION_CREATE + "')")
    public ResponseEntity<String> adminGetRequest() {
        return ResponseEntity.ok("ADMIN ROLE - bober");
    }

    @GetMapping
    @RequestMapping("/user") // ToDo: @GetMapping("/user")
    @PreAuthorize("hasAuthority('user:read')") // ToDo: example - @PreAuthorize("hasRole('" + IP_CONFIGURATION_CREATE + "')")
    public ResponseEntity<String> userGetRequest() {
        return ResponseEntity.ok("USER ROLE - kurwa");
    }

}