package auth_submodule.dev.email;

public interface EmailService {
    void send(String to, String email);
}