package auth_submodule.dev.email;

import auth_submodule.common.constants.ExceptionConstants;
import auth_submodule.dev.exceptions.EmailNotSentException;
import auth_submodule.dev.exceptions.SetMessageTextException;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TestMailServiceImpl implements EmailService { // ToDo: rename to MailDevService

    private final static Logger LOGGER = LoggerFactory.getLogger(TestMailServiceImpl.class); // ToDo: remind why we are not using @Slf4j

    private final JavaMailSender mailSender;

    @Override
    public void send(String to, String message) {

        MimeMessage mimeMessage = mailSender.createMimeMessage(); // ToDo: remove redundant lines

        configureMessage(mimeMessage, to, message);

        mailSender.send(mimeMessage);
    }

    private void configureMessage(MimeMessage mimeMessage, String to, String message) {
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8"); // ToDo: move to MailConstants
        try {
            helper.setText(message, true);
            helper.setTo(to);
            helper.setSubject("Confirm your email");
            helper.setFrom("bebra@gmail.com");
        } catch (MailException e) {
            LOGGER.error(ExceptionConstants.FAILED_TO_SEND_EMAIL, e);
            throw new EmailNotSentException(ExceptionConstants.FAILED_TO_SEND_EMAIL);
        } catch (MessagingException e) {
            LOGGER.error(ExceptionConstants.FAILED_TO_SET_MESSAGE_TEXT); // ToDo: explain this exception
            throw new SetMessageTextException(ExceptionConstants.FAILED_TO_SET_MESSAGE_TEXT);
        }
    }
}