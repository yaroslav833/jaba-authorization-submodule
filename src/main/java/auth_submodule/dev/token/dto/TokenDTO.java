package auth_submodule.dev.token.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor // ToDo: pyramid
public class TokenDTO {

    @JsonProperty("access_token") // ToDo: do we need names in @JsonProperty?
    private String accessToken;

    @JsonProperty("refresh_token") // ToDo: do we need names in @JsonProperty?
    private String refreshToken;

}