package auth_submodule.dev.token.entities;

import auth_submodule.common.constants.DatabaseConstants;
import auth_submodule.dev.user.entity.UserEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = DatabaseConstants.REFRESH_TOKEN_TABLE_NAME)
public class RefreshTokenEntity {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY // ToDo: similar comments as in AccessTokenEntity
    )
    private Long id;

    private String token;

    private boolean expired;

    private boolean revoked;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Column(name = "expires_at", nullable = false)
    private LocalDateTime expiresAt;

    @PrePersist
    public void setExpiresAt() {
        expiresAt = LocalDateTime.now().plusSeconds(86400);
    } // ToDo: rename to prePersist, probably need to add @PreUpdate

}