package auth_submodule.dev.token.entities;

import auth_submodule.common.constants.DatabaseConstants;
import auth_submodule.dev.user.entity.UserEntity;
import jakarta.persistence.*;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = DatabaseConstants.ACCESS_TOKEN_TABLE_NAME)
public class AccessTokenEntity {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY // ToDo: to one line
    )
    private Long id;

    private String token; // ToDo: @Column, nullable = false

    private boolean expired; // ToDo: @Column, nullable = false

    private boolean revoked; // ToDo: @Column, nullable = false

    @ManyToOne
    @JoinColumn(name = "user_id") // ToDo: nullable = false
    private UserEntity user;

}