package auth_submodule.dev.token.entities;

import auth_submodule.common.constants.DatabaseConstants;
import auth_submodule.dev.token.entities.listeners.ConfirmationTokenEntityListener;
import auth_submodule.dev.user.entity.UserEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * The fields {@link createdAt} and {@link expiresAt} are set in // ToDo: errors in variables
 * {@link ConfirmationTokenEntityListener} callback handler.
 */
@Getter
@Setter
@NoArgsConstructor
@Entity(name = DatabaseConstants.CONFIRMATION_TOKEN_TABLE_NAME) // ToDo: pyramid
@EntityListeners(ConfirmationTokenEntityListener.class)
public class ConfirmationTokenEntity {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY // ToDo: one line
    )
    private Long id;

    @Column(name = "token", nullable = false)
    private String token;

    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;

    @Column(name = "expires_at", nullable = false)
    private LocalDateTime expiresAt;

    @Column(name = "confirmed_at")
    private LocalDateTime confirmedAt;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity userEntity;

    public ConfirmationTokenEntity(String token,
                                   UserEntity userEntity) {
        this.token = token;
        this.userEntity = userEntity;
    }

}