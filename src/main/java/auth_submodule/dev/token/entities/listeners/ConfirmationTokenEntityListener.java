package auth_submodule.dev.token.entities.listeners;

import auth_submodule.common.ansi.ANSI;
import auth_submodule.dev.token.entities.ConfirmationTokenEntity;
import jakarta.persistence.PrePersist;

import java.time.LocalDateTime;

public class ConfirmationTokenEntityListener {

    @PrePersist
    private void beforeAnyPersist(ConfirmationTokenEntity confirmationTokenEntity) { // ToDo: move method to ConfirmationTokenEntity, remind if we need @PreUpdate here
        var creationTime = LocalDateTime.now();
        confirmationTokenEntity.setCreatedAt(creationTime);
        confirmationTokenEntity.setExpiresAt(creationTime.plusMinutes(15));
        System.out.println(ANSI.ANSI_GREEN + "created at: " + confirmationTokenEntity.getCreatedAt() + ANSI.ANSI_RESET); // @Slf4j instead of sout, message can be moved constants
        System.out.println(ANSI.ANSI_RED + "expires at: " + confirmationTokenEntity.getExpiresAt() + ANSI.ANSI_RESET);
    }

}