package auth_submodule.dev.token.repositories;

import auth_submodule.dev.token.entities.AccessTokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccessTokenRepository extends JpaRepository<AccessTokenEntity, Integer> { // ToDo: change to Long

    // ToDo: try to change to List<RefreshTokenEntity> findAllByUserIdAndExpiredFalseOrRevokedFalse(Integer userId); // ToDo: change to Long and remove @Query
    @Query("""
            select t from access_token t inner join registration r on t.user.id = r.id
            where r.id = :userId and (t.expired = false or t.revoked = false)
            """)
    List<AccessTokenEntity> findAllValidTokensByUser(Integer userId); // ToDo: change to Long

    Optional<AccessTokenEntity> findByToken(String token);  // ToDo: indent, remove redundant line

}