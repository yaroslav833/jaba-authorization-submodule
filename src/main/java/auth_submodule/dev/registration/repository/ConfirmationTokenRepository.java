package auth_submodule.dev.registration.repository;

import auth_submodule.dev.token.entities.ConfirmationTokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Repository
public interface ConfirmationTokenRepository extends JpaRepository<ConfirmationTokenEntity, Long> {

    Optional<ConfirmationTokenEntity> findByToken(String token);

    @Modifying
    @Transactional
    @Query("update confirmation_token c set c.confirmedAt = ?2 where c.token = ?1")
    void updateConfirmedAt(String token, LocalDateTime now);

}