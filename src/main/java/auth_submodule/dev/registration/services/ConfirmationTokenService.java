package auth_submodule.dev.registration.services;

import auth_submodule.dev.registration.repository.ConfirmationTokenRepository;
import auth_submodule.dev.token.entities.ConfirmationTokenEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ConfirmationTokenService {

    private final ConfirmationTokenRepository tokenRepository;

    public Optional<ConfirmationTokenEntity> getToken(String token) {
        return tokenRepository.findByToken(token);
    }

    public void saveConfirmationToken(ConfirmationTokenEntity token) { // ToDo: or write getConfirmationToken, or write saveToken
        tokenRepository.save(token);
    }

    public void setConfirmedAt(String token) { // ToDo: rename to confirmToken
        tokenRepository.updateConfirmedAt(token, LocalDateTime.now());
    }

}