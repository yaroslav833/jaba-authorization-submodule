package auth_submodule.dev.registration.services;

import auth_submodule.common.constants.ExceptionConstants;
import auth_submodule.common.security.UserRole;
import auth_submodule.common.signin.services.SignInService;
import auth_submodule.dev.token.dto.TokenDTO;
import auth_submodule.dev.authentication.services.JwtService;
import auth_submodule.dev.email.EmailService;
import auth_submodule.dev.exceptions.EmailAlreadyConfirmedException;
import auth_submodule.dev.exceptions.TokenExpiredException;
import auth_submodule.dev.registration.controllers.dto.RegistrationRequest;
import auth_submodule.dev.registration.mapper.UserMapper;
import auth_submodule.dev.templates.EMailHelper;
import auth_submodule.dev.token.repositories.AccessTokenRepository;
import auth_submodule.dev.token.entities.ConfirmationTokenEntity;
import auth_submodule.dev.token.repositories.RefreshTokenRepository;
import auth_submodule.dev.user.entity.UserEntity;
import auth_submodule.dev.user.services.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
public class RegistrationService extends SignInService {

    private final UserMapper userMapper; // ToDo: remove redundant lines

    private final UserService userService;

    private final EmailService testMailServiceImpl;

    private final ConfirmationTokenService confirmationTokenService;

    public RegistrationService( // ToDo: @RequiredArgsConstructor
            JwtService jwtService,
            UserMapper userMapper,
            UserService userService,
            AccessTokenRepository accessTokenRepository,
            EmailService testMailServiceImpl,
            ConfirmationTokenService confirmationTokenService,
            RefreshTokenRepository refreshTokenRepository)
    {
        super(jwtService, accessTokenRepository, refreshTokenRepository);
        this.userMapper = userMapper;
        this.userService = userService;
        this.testMailServiceImpl = testMailServiceImpl;
        this.confirmationTokenService = confirmationTokenService;
    }

    public TokenDTO register(RegistrationRequest request) {
        final UserEntity user = userMapper.toEntity(request, UserRole.USER);
        final String confirmationToken = userService.signUpUser(user); // ToDo: not sure about the name of a method

        final String email = request.getEmail();
        final String message = EMailHelper.formRegistrationMessage(email, confirmationToken);

        testMailServiceImpl.send(email, message); // ToDo: line 55-58 to separate method sendRegistrationEmail or smth like that

        // ToDo: final String refreshToken = getRefreshToken(user);
        // ToDo: final String jwtToken = jwtService.generateAuthorizationToken(user);

        final String refreshToken = jwtService.generateRefreshToken(user);
        final String jwtToken = getRefreshToken(user); // ToDo: here we should have refresh token? what about above?

        revokeAllAccessTokens(user);
        saveUserToken(user, jwtToken);

        return TokenDTO.builder() // ToDo: to separate mapper
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .build();
    }

    @Transactional
    public String confirmToken(String token) {
        ConfirmationTokenEntity confirmationTokenEntity = confirmationTokenService
                .getToken(token)
                .orElseThrow(() ->
                        new IllegalStateException("token not found")); // ToDo: to line higher

        validateToken(confirmationTokenEntity);

        confirmationTokenService.setConfirmedAt(token);
        userService.enableAppUser(confirmationTokenEntity.getUserEntity().getEmail()); // ToDo: probably you mean here authorize user

        return "Token " + confirmationTokenEntity.getToken() + " confirmed successfully :3"; // ToDo: to separate message constants
    }

    private void validateToken(ConfirmationTokenEntity confirmationTokenEntity) {
        if (confirmationTokenEntity.getExpiresAt().isBefore(LocalDateTime.now())) {
            throw new TokenExpiredException(ExceptionConstants.TOKEN_EXPIRED);
        }
        if (confirmationTokenEntity.getConfirmedAt() != null) {
            throw new EmailAlreadyConfirmedException(ExceptionConstants.EMAIL_ALREADY_CONFIRMED);
        }
    }

}