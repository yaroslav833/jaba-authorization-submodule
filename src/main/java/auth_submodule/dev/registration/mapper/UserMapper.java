package auth_submodule.dev.registration.mapper;

import auth_submodule.dev.registration.controllers.dto.RegistrationRequest;
import auth_submodule.common.security.UserRole;
import auth_submodule.dev.user.entity.UserEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserMapper { // ToDo: there is a separate package related to user, maybe it should be there?

    public UserEntity toEntity(RegistrationRequest request, UserRole userRole) {
        return new UserEntity(
                request.getFirstName(),
                request.getLastName(),
                request.getEmail(),
                request.getPassword(),
                userRole
        );
    }

}
