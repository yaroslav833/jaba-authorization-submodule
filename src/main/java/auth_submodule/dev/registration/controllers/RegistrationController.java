package auth_submodule.dev.registration.controllers;

import auth_submodule.dev.token.dto.TokenDTO;
import auth_submodule.dev.registration.controllers.dto.RegistrationRequest;
import auth_submodule.dev.registration.services.RegistrationService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated // ToDo: remind why do we need this annotation?
@RestController
@RequestMapping(path = "/api/v1/registration") // ToDo: pyramid
@RequiredArgsConstructor
public class RegistrationController {

    private final RegistrationService registrationService;

    @PostMapping
    public ResponseEntity<TokenDTO> register(@Valid @RequestBody RegistrationRequest request) {
        return ResponseEntity.ok(registrationService.register(request));  // ToDo: extract result of .register() to variable
    }

    @GetMapping(path = "/confirm")
    public String confirm(@RequestParam("token") String token) { // ToDo: redundant name in request param
        return registrationService.confirmToken(token);
    }

}