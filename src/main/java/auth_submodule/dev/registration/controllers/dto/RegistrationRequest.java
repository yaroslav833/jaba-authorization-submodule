package auth_submodule.dev.registration.controllers.dto;

import auth_submodule.common.constants.RegularExpressionConstants;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class RegistrationRequest { // ToDo: rename to RegistrationDTO

    @NotBlank
    private String firstName; // ToDo: remove redundant line between firstName and lastName

    @NotBlank
    private String lastName;

    @Pattern(regexp = RegularExpressionConstants.PASSWORD_REGEX,
            message = RegularExpressionConstants.PASSWORD_NOT_ACCEPTED) // ToDo: in one line
    private String password;

    @Email(regexp = RegularExpressionConstants.RFC_5322_REGEX,
            message = RegularExpressionConstants.EMAIL_NOT_VALID) // ToDo: in one line, move email higher than password
    private String email;

}