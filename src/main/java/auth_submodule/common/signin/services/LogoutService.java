package auth_submodule.common.signin.services;

import auth_submodule.dev.token.repositories.AccessTokenRepository;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;

// ToDo: move services package with LogoutService to security package.
@Service
@RequiredArgsConstructor
public class LogoutService implements LogoutHandler {

    private final AccessTokenRepository accessTokenRepository; // ToDo: common uses dev's repository, I think all logic can be covered under one auth_submodule, no need to divide it to common and dev

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        final String authHeader = request.getHeader("Authorization"); // ToDo: move to auth constants
        final String jwt; // ToDo: inline to line 24
        if (authHeader == null || !authHeader.startsWith("Bearer ")) { // ToDo: move to auth constants, extract to method - validateAuthHeader(), move the method to AuthorizationUtils
            return;
        }
        jwt = authHeader.substring(7); // ToDo: move to AuthorizationUtils, extract to method extractJwtToken()
        var storedToken = accessTokenRepository.findByToken(jwt) // ToDo: .findByToken to new line, final, rename to token
                .orElse(null);
        if(storedToken != null) { // ToDo: Ctrl+Alt+L, extract condition logic to method revokeToken()
            storedToken.setExpired(true);
            storedToken.setRevoked(true);
            accessTokenRepository.save(storedToken);
        }
    }

}