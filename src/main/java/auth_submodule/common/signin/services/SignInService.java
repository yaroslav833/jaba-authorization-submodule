package auth_submodule.common.signin.services;

import auth_submodule.dev.authentication.services.JwtService;
import auth_submodule.dev.token.entities.AccessTokenEntity;
import auth_submodule.dev.token.repositories.AccessTokenRepository;
import auth_submodule.dev.token.entities.RefreshTokenEntity;
import auth_submodule.dev.token.repositories.RefreshTokenRepository;
import auth_submodule.dev.user.entity.UserEntity;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Optional;

// ToDo: remove SignInService nahoooooooi
@RequiredArgsConstructor
public class SignInService {

    protected final JwtService jwtService; // ToDo: remove redundant lines

    protected final AccessTokenRepository accessTokenRepository;

    protected final RefreshTokenRepository refreshTokenRepository;

    // ToDo: add @Scheduler for revocation of invalid tokens, put 15 minutes to scheduler, this method here is not needed anymore, move to AccessTokenService
    protected void revokeAllAccessTokens(UserEntity user) {
        var validUserTokens = accessTokenRepository.findAllValidTokensByUser(user.getId().intValue()); // ToDo: remove .intValue()
        if (validUserTokens.isEmpty()) {
            return;
        }

        validUserTokens.forEach(t -> { // ToDo: extract logic to method revokeTokens()
            t.setExpired(true);
            t.setRevoked(true);
        });
        accessTokenRepository.saveAll(validUserTokens);
    }

    // ToDo: move to JWTService, move to AccessTokenService
    protected void saveUserToken(UserEntity user, String jwtToken) {
        final var token = AccessTokenEntity.builder()
                .user(user)
                .token(jwtToken)
                .expired(false)
                .revoked(false)
                .build();
        accessTokenRepository.save(token);
    }

    /**
     * Retrieve a refresh token if exists.
     * In case it does not exist or is expired/invalid - create new
     * refresh token and save it to repository.
     *
     * @param user - {@link UserEntity} associated with this refresh token
     * @return - existing token from repository or created and saved new one
     */

    // ToDo: move to RefreshTokenService
    protected String getRefreshToken(UserEntity user) { // ToDo: rename to retrieveRefreshToken
        var tokenList = refreshTokenRepository.findAllValidRefreshTokensByUser(user.getId().intValue()); // ToDo: .intValue() - remove, rename to refreshTokens
        Optional<RefreshTokenEntity> latestRefreshToken = tokenList // ToDo: rename latestRefreshTokenOpt
                .stream()
                .max(Comparator.comparing(RefreshTokenEntity::getExpiresAt));
        if (latestRefreshToken.isPresent()) { // ToDo: invert condition, inside condition - create token, outside - get token
            final var token = latestRefreshToken.get(); // ToDo: rename to latestRefreshToken
            final LocalDateTime latestExpirationDate = token.getExpiresAt(); // ToDo: redundant variable
            if (latestExpirationDate.isAfter(LocalDateTime.now()) && jwtService.isTokenValid(token.getToken(), user)) { // ToDo: extract to validateRefreshToken()
                return token.getToken();
            }
        }

        return createNewRefreshToken(user);
    }

    // ToDo: rename to createRefreshToken(), move to RefreshTokenService
    /*
    private String createNewRefreshToken(UserEntity user) {
        revokeAllRefreshTokens(user);

        var token = jwtService.generateRefreshToken(user);
        saveRefreshToken(user, token);
        return token;
    }
     */
    private String createNewRefreshToken(UserEntity user) {
        var token = jwtService.generateRefreshToken(user);

        revokeAllRefreshTokens(user);
        saveRefreshToken(user, token);
        return token;
    }

    // ToDo: move to RefreshTokenService
    private void revokeAllRefreshTokens(UserEntity user) {
        var validUserTokens = refreshTokenRepository.findAllValidRefreshTokensByUser(user.getId().intValue()); // ToDo: remove .intValue()
        if (validUserTokens.isEmpty()) {
            return;
        }

        validUserTokens.forEach(t -> { // ToDo: extract logic to method revokeTokens()
            t.setExpired(true);
            t.setRevoked(true);
        });
        refreshTokenRepository.saveAll(validUserTokens);
    }

    // ToDo: move to RefreshTokenService
    private void saveRefreshToken(UserEntity user, String refreshToken) {
        final var token = RefreshTokenEntity.builder() // ToDo: delegate token creation to TokenMapper, with method toEntity(UserEntity user, String refreshToken)
                .user(user)
                .token(refreshToken)
                .expired(false)
                .revoked(false)
                .build();
        refreshTokenRepository.save(token);
    }

}