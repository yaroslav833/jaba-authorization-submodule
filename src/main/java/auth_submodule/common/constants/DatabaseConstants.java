package auth_submodule.common.constants;

public class DatabaseConstants { // ToDo: remove lines 4 and 12, and in between fields

    public static final String REGISTRATION_TABLE_NAME = "registration";

    public static final String CONFIRMATION_TOKEN_TABLE_NAME = "confirmation_token";

    public static final String ACCESS_TOKEN_TABLE_NAME = "access_token";

    public static final String REFRESH_TOKEN_TABLE_NAME = "refresh_token";

}