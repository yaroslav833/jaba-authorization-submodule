package auth_submodule.common.constants;

public class ExceptionConstants { // ToDo: the same as in Database Constants

    public static final String FAILED_TO_SEND_EMAIL = "failed to send email";

    public static final String FAILED_TO_SET_MESSAGE_TEXT = "failed to set message text";

    public static final String USER_NOT_FOUND_MESSAGE = "user with email %s not found";

    public static final String EMAIL_ALREADY_TAKEN = "email already taken";

    public static final String EMAIL_ALREADY_CONFIRMED = "email already confirmed";

    public static final String TOKEN_EXPIRED = "token expired";

}
