package auth_submodule.common.constants;

public class RegularExpressionConstants { // ToDo: the same as DatabaseConstants

    //region Regular expressions // ToDo: good job with discovering! thnx! now you can remove it :З
    public static final String RFC_5322_REGEX = "^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";

    public static final String PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[!@#$%^&*()_+|~-]).{8,}$";
    //endregion  // ToDo: good job with discovering! thnx! now you can remove it :З

    //region Regular expression responses  // ToDo: good job with discovering! thnx! now you can remove it :З
    public static final String PASSWORD_NOT_ACCEPTED = "Provided password does not match the requirements";

    public static final String EMAIL_NOT_VALID = "Provided string is not an email";
    //endregion  // ToDo: good job with discovering! thnx! now you can remove it :З

}
