package auth_submodule.common.security.configuration;

import auth_submodule.dev.authentication.JwtAuthenticationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class WebSecurityConfiguration {

    private static final String[] WHITE_LIST_URL = {"/api/v1/auth/**", "/api/v1/registration/**"}; // ToDo: each link to separate line, move WHITE_LIST_URL to separate constants class related to request matchers

    private final JwtAuthenticationFilter jwtAuthenticationFilter; // ToDo: remove redundant intend below

    private final AuthenticationProvider authenticationProvider; // ToDo: remove redundant intend below

    private final LogoutHandler logoutHandler; // ToDo: remove redundant intend below, make classes pyramid way

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception { // ToDo: write custom exception that handles errors related to filter chain configuration

        http
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(req ->  // ToDo: requestMatchers - permitAll, next line - anyRequest - auth
                        req.requestMatchers(WHITE_LIST_URL)
                                .permitAll()
                                .anyRequest()
                                .authenticated()
                ) // ToDo: extract authorize http requests handling to separate method
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS)) // ToDo: move after .csrf()
                .authenticationProvider(authenticationProvider) // ToDo: move one-liners above, then put authorizeHttpRequests(), then logout()
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class) // ToDo: addFilter(jwtAuthenticationFilter) - try to run this
                .logout(logout -> logout
                        .logoutUrl("/api/v1/auth/logout") // ToDo: move to constants
                        .addLogoutHandler(logoutHandler)
                        .logoutSuccessHandler(
                                ((request, response, authentication) -> SecurityContextHolder.clearContext()) // ToDo: remove redundant parenthesis
                        )
                );  // ToDo: extract logout handling to separate method

        return http.build();
    }

}