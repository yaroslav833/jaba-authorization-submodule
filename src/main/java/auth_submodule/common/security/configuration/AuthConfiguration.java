package auth_submodule.common.security.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;

@Configuration
@EnableWebSecurity // ToDo: remove enable web security annotation and check if it's working
@RequiredArgsConstructor
public class AuthConfiguration {

    private final BCryptPasswordEncoder passwordEncoder; // ToDo: change to PasswordEncoder and check if it's working

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider(UserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setPasswordEncoder(passwordEncoder);
        authProvider.setUserDetailsService(userDetailsService);
        return authProvider;
    }

    @Bean
    public AuthenticationManager authenticationManager(UserDetailsService userDetailsService) {
        // ToDo: remove Collections.singletonList() and check if it works.
        // ToDo -> Optional: split daoAuthenticationProvider to separate variable and put the variable within ProviderManager().
        return new ProviderManager(Collections.singletonList(daoAuthenticationProvider(userDetailsService, passwordEncoder)));
    }

}