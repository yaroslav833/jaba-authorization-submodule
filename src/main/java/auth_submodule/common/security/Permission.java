package auth_submodule.common.security;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

// ToDo: check two approaches, decide which one fits better (I insist on checking RoleHierarchy approach), move to enums package
@RequiredArgsConstructor
public enum Permission { // ToDo: change type of class to public static final String and use the constant name in PreAuthorize methods, move to constants package and to Constants class

    ADMIN_CREATE("admin:create"), // ToDo: for ADMIN you should have only permission - ADMIN, remove _CREATE, _READ, etc.
    ADMIN_READ("admin:read"),
    ADMIN_UPDATE("admin:update"),
    ADMIN_DELETE("admin:delete"),
    USER("user:all"),
    USER_CREATE("user:create"),
    USER_READ("user:read"),
    USER_UPDATE("user:update"),
    USER_DELETE("user:delete");

    @Getter
    private final String permission; // ToDo: delete

}