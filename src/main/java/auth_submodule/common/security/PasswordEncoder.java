package auth_submodule.common.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class PasswordEncoder { // ToDo: change name to PasswordEncoderConfiguration, move to configuration package

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    } // ToDo: try to change return type to PasswordEncoder and see if it's working

}