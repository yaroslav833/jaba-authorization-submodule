package auth_submodule.common.ansi;

/**
 * To colorize the output String it is necessary to concatenate it with:
 * 1 - prefix with a specified color;
 * 2- suffix with reset color {@link ANSI_RESET} // ToDo: is it working? IntelliJ shows that it cannot resolve symbol. format the spaces as well
 */
public class ANSI { // ToDo: add indent, rename to ANSIConstants, add package to constants with name logger
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";

    public static final String ANSI_RED_BACKGROUND = "\u001B[41m"; // ToDo: is it needed?
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m"; // ToDo: is it needed? + add indent
}
