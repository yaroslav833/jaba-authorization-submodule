package auth_submodule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthSubmoduleApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthSubmoduleApplication.class, args);
    }

}