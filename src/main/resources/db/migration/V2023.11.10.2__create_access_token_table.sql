create table if not exists access_token
(
    id         bigint generated by default as identity not null,
    token      varchar                                 not null,
    token_type varchar                                 not null,
    expired    boolean                                 not null,
    revoked    boolean                                 not null,
    user_id    bigint, -- ToDo: not null
    foreign key (user_id) references registration (id),
    constraint pk_access_token primary key (id)
); -- ToDo: all scripts merge to one