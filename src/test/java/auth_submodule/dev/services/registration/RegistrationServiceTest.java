package auth_submodule.dev.services.registration;

import auth_submodule.common.ansi.ANSI;
import auth_submodule.dev.registration.mapper.UserMapper;
import auth_submodule.dev.email.EmailService;
import auth_submodule.dev.registration.services.ConfirmationTokenService;
import auth_submodule.dev.user.services.UserService;
import auth_submodule.common.security.UserRole;
import auth_submodule.dev.user.entity.UserEntity;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RegistrationServiceTest {

    private UserMapper userMapper;

    private UserService userService;

    private EmailService emailService;

    private ConfirmationTokenService confirmationTokenService;

/*    @BeforeEach
    void setUp() {
        userService = new UserService()
    }*/


    @Test
    void register() {
        //given
        UserEntity entity = new UserEntity(
                "Lokir",
                "Horse Thief",
                "saygex@dodixted.whtrn",
                "ABCDEFGKLM82%",
                UserRole.USER
                );
        String token = "228";// userService.signUpUser(entity);
        System.out.println(ANSI.ANSI_GREEN + "user token = " + token + " " + ANSI.ANSI_RESET);
    }

    @Test
    @Disabled
    void confirmToken() {
    }
}